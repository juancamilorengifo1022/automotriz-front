import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PositionService } from 'src/app/services/position.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styles: [
  ]
})
export class PositionComponent  {
  public formSubmit = false;

  public positionForm = this.fb.group({
    carnombre : ['', [Validators.required, Validators.minLength(3)]  ]
  });


  constructor(private fb:FormBuilder,
    private positionService: PositionService) { }

  crearCargo(){
    this.formSubmit =  true;
    if(this.positionForm.invalid){
      return;
    }
    this.positionService.crearCargo(this.positionForm.value)
    .subscribe( resp => {
      Swal.fire({
        title: 'Proceso Exitoso',
        text: 'Cargo registrado satisfactoriamente',
        icon: 'success'
      });
    }, (err) => {
      Swal.fire({
        title: 'Error!',
        text: 'Error al guardar',
        icon: 'error'
      });
    });
  }

  campoNoValido( campo: string) : boolean {
    if(this.positionForm.get(campo)?.invalid && this.formSubmit){
      return true;
    } else{
      return false;
    }

  }


}
