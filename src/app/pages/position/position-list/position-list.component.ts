import { Component, OnInit } from '@angular/core';
import { Position } from 'src/app/models/position.model';
import { PositionService } from '../../../services/position.service';


@Component({
  selector: 'app-position-list',
  templateUrl: './position-list.component.html',
  styles: [
  ]
})
export class PositionListComponent implements OnInit {

  public positions: Position[] = [];



  constructor(private positionService: PositionService ) { }

  ngOnInit(): void {
    this.cargarCargos();
  }

  cargarCargos() {
    this.positionService.cargarCargos().subscribe((data: any ) => {
      this.positions = data;
    });
  }



}
