import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/product.service';
import { Product } from '../../../models/product.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService ) { }
  public products: Product[] = [];

  ngOnInit(): void {
    this.cargarProductos();
  }

  cargarProductos(){
    this.productService.cargarProductos().subscribe((data: any ) => {
      this.products = data;
    });
  }


}
