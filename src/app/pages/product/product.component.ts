import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styles: [
  ]
})
export class ProductComponent  {

  public formSubmit = false;

  public productForm = this.fb.group({
    pronombre : ['', [Validators.required, Validators.minLength(5)]  ],
    procantidad : ['', Validators.required  ],
    pronewfec   : ['', Validators.required],
    pronewuser  : ['', Validators.required]
  });

  constructor(private fb:FormBuilder,
    private productService: ProductService) { }

  crearProducto(){
    this.formSubmit =  true;
    if(this.productForm.invalid){
      return;
    }
    this.productService.crearProducto(this.productForm.value)
    .subscribe( resp => {
      console.log('usuario creado');
      console.log(resp);
    }, (err) => console.warn(err));
  }

  campoNoValido( campo: string) : boolean {
    if(this.productForm.get(campo)?.invalid && this.formSubmit){
      return true;
    } else{
      return false;
    }

  }



}
