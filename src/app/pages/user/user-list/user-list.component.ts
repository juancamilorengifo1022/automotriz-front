import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user.model';
import { UserService } from '../../../services/user.service';
import { cargarUsuarios } from '../../../interfaces/register-form-interfaces';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styles: [
  ]
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserService ) { }
  public users: User[] = [];

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  cargarUsuarios(){
    this.userService.cargarUsuarios().subscribe((data: any ) => {
      this.users = data;
    });
  }

}
