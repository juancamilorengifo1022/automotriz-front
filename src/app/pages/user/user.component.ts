import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: [
  ]
})
export class UserComponent {

  public userForm = this.fb.group({
    usucodigo : ['', [Validators.required, Validators.minLength(3)]  ],
    usunombre : ['', [Validators.required, Validators.minLength(3)]  ],
    usuedad   : ['', Validators.required],
    usucargo  : ['', Validators.required]
  });

  constructor(private fb:FormBuilder){}

  crearUsuaio(){
    console.log(this.userForm.value);
  }



}
