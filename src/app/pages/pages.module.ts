import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductComponent } from './product/product.component';
import { UserComponent } from './user/user.component';
import { PositionComponent } from './position/position.component';
import { PagesComponent } from './pages.component';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { ProductListComponent } from './product/product-list/product-list.component';
import { PositionListComponent } from './position/position-list/position-list.component';
import { UserListComponent } from './user/user-list/user-list.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ProductComponent,
    UserComponent,
    PositionComponent,
    PagesComponent,
    ProductListComponent,
    PositionListComponent,
    UserListComponent
  ],
  exports: [
    DashboardComponent,
    ProductComponent,
    UserComponent,
    PositionComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class PagesModule { }
