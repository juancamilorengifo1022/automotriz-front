import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component';
import { ProductComponent } from './product/product.component';
import { PositionComponent } from './position/position.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { PositionListComponent } from './position/position-list/position-list.component';



const routes: Routes = [
  { path: 'dashboard', component: PagesComponent,
  children: [
    { path: '', component: DashboardComponent },
    { path: 'user', component: UserComponent },
    { path: 'user-list', component: UserListComponent },

    { path: 'position', component: PositionComponent },
    { path: 'position-list', component: PositionListComponent },

    { path: 'product', component: ProductComponent },
    { path: 'product-list', component: ProductListComponent },

    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
