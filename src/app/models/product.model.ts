
export class Product {

  constructor(
    public procodigo: number,
    public pronombre: string,
    public procantidad: number,
    public pronewuser: string,
    public pronewfec: Date ,
    public promoduser?: string ,
    public promodfec?: Date ,
  ){}

}
