import { Injectable } from '@angular/core';
import { RegisterFormProduct, cargarProductos } from '../interfaces/register-form-interfaces';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Observable } from 'rxjs';

const base_url = environment.base_url;
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  crearProducto( formData: RegisterFormProduct){
    return this.http.post(`${ base_url}/product/save`, formData);
  }

  public cargarProductos():  Observable<cargarProductos>{
    return this.http.get<cargarProductos>(`${ base_url}/product/all`);
  }
}
