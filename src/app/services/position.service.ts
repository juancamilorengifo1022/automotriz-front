import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { RegisterFormPosition, cargarCargos } from '../interfaces/register-form-interfaces';
import { HttpClient } from '@angular/common/http';
import { Position } from '../models/position.model';
import { Observable } from 'rxjs';

const base_url = environment.base_url;
@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor(private http: HttpClient) { }

  crearCargo( formData: RegisterFormPosition){
    return this.http.post(`${ base_url}/position/save`, formData);
  }

  public cargarCargos():  Observable<cargarCargos>{
    return this.http.get<cargarCargos>(`${ base_url}/position/all`);
  }
}
