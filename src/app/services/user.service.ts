import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterFormUser, cargarUsuarios } from '../interfaces/register-form-interfaces';
import { environment } from '../../environments/environment.prod';
import { Observable } from 'rxjs';

const base_url = environment.base_url;
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  crearUsuario( formData: RegisterFormUser){
    return this.http.post(`${ base_url}/user/save`, formData);
  }

  public cargarUsuarios():  Observable<cargarUsuarios>{
    return this.http.get<cargarUsuarios>(`${ base_url}/user/all`);
  }


}
