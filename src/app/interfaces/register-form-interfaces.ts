import { Position } from '../models/position.model';
import { User } from '../models/user.model';
import { Product } from '../models/product.model';
export interface RegisterFormUser {
  usucodigo : string;
  usunombre : string;
  usuedad   : any;
  usucargo  : any;
}

export interface RegisterFormProduct {
  pronombre   : string;
  procantidad : any;
  pronewfec   : Date;
  pronewuser  : string;
}

export interface RegisterFormPosition {
  carnombre : string;
}

export interface cargarCargos {
  cargos : Position[];
}

export interface cargarUsuarios {
  usuarios : User[];
}

export interface cargarProductos {
  productos : Product[];
}
